native class System
{

	static public var exactSettings : bool;
	static public var useCodePage : bool;

	static function setClipboard( s : String ) : bool;
	static function showSettings( panel : int ) : void;

	static var security : {
		allowDomain: 'a // String -> void
		allowInsecureDomain: 'a // String -> void
		localPolicyFile: 'a // String -> void
		sandboxType : String
	};

	// CALLBACKS
//	function onStatus( ??? ) : void;       // unknown parameters...

}
